Workflow
==============
**Generally, a workflow will:**  

1. Prepare a pbs file containing schedular information  
2. Run a function on the parsed text file  

Shown below is an example of an RNA-seq sample setup.

**Step1:** Prepare PBS file 
```bash
./PBSPrep.sh -N CutRun_Alignment -o CutRun_Alignment.out -e CutRun_Alignment.err -l 1 -p 5 -w 01:00:00 -t 4
```

The command shown above will produce the following text file:  
  

*CutRun_Alignment_Example.pbs*  
```
#PBS -N CutRun_Alignment 
#PBS -o ${PBS_O_WORKDIR}/CutRun_Alignment.out  
#PBS -e ${PBS_O_WORKDIR}/CutRun_Alignment.err  
#PBS -l nodes=1:ppn=5  
#PBS -l walltime=01:00:00  
#PBS -t 1-4  
```

**Step2:** Once the pbs file is prepared, we add scripts to run an analysis:  
  

*CutRun_Alignment.pbs* 
```
#PBS -N CutRun_Alignment_Example
#PBS -o ${PBS_O_WORKDIR}/CutRun_Alignment.out  
#PBS -e ${PBS_O_WORKDIR}/CutRun_Alignment.err  
#PBS -l nodes=1:ppn=5  
#PBS -l walltime=01:00:00  
#PBS -t 1-4  

source <path>/./Aligners_v2.0.sh                                # Sourcing Alignment Script
mm10                                                            # Calling mm10 function
bowtie2_cutrun <pathToFile> <single;paired> <sampleFile.txt>    # Running STAR Alignment
```

Sample Files
==============
In order to process the samples, I have found it easiest to keep a file named sampleFiles.txt in the same directory as the samples. For example, the sampleFiles.txt should be in the same directory as the .fastq.qz files. 

SampleFiles.txt is a space seperated file containing the same names. To generate the file, it is best to use:  
```
ls *.fastq.gz | xargs -n+2
```

Aligners_v2.0.sh
==============
This script contains a set of functions to run alignments using STAR, Hisat2, BWA-mem, bowtie2, and kallisto. Each of the aligners utilize the fileParser_v2.sh script in order to prepare the files for alignment.

*STAR Alignment Functions*
```
STAR_Align <pathtoSamples> <single;paired> <sampleFile.txt>        # Aligns using STAR
STAR_TEUniq <pathtoSamples> <single;paired> <sampleFile.txt>       # Aligns using STAR; best for transposable element alignment
```

*HISAT2 Alignment Functions*
```
Hisat2_Align <pathtoSamples> <single;paired> <sampleFile.txt>      # Alignssamples to genome using Hisat2 with default parameters
```

*bwa-mem Alignment Functions*
```
BWAmem_align <pathtoSamples> <single;paired> <sampleFile.txt>      # Aligns samples to genome using bwa-mem with default parameters
```

*bowtie2 Alignment Functions*
```
bowtie2_Align <pathtoSamples> <single;paired> <sampleFile.txt>      # Aligns Samples to genome using default bowtie2 parameters
bowtie2_cutrun <pathtoSamples> <single;paired> <sampleFile.txt>     # Aligns samples to genome using bowtie2 parameters best suited for cut&Run or cut&Tag data
```

