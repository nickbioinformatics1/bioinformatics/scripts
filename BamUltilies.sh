#!/usr/bin/bash

source /home/nick.pierce/Fresh_Scripts/./FileParser_v2.0.sh

#######################
# FUNCTIONS
#######################
extract_reads () {
	base=$(pwd)

	extract_reads_usage () { echo "Extract_Reads"
	"Usage $0 -n [dir_name] -f [samtools view flag] -t [threads] -d [Dir Location of Samples_file.txt] -a [Analysis_Type:single or paired]"
	1>&2
	exit 1
	}
	
	while getopts ":n:f:t:d:a:" opt; do
		case "${opt}" in
			n) DIR_NAME=${OPTARG} ;;
			f) FLAG=${OPTARG} ;;
			t) THREADS=${OPTARG} ;;
			d) FILE_DIR=${OPTARG} ;;
			a) ANALYSIS_TYPE=${OPTARG} ;;
			:) 
				echo "Error: -${OPTARG} requires argument"
				exit 1 ;;
			*) extract_reads_usage ;;
		esac
	done
	shift $((OPTIND -1))

	[ -d ${DIR_NAME} ] || mkdir ${DIR_NAME}
	Sample_input ${FILE_DIR} ${ANALYSIS_TYPE}

	echo -e "\n#################################"
	echo -e "Extract Reads Log"
	echo -e	"#################################"
	echo "Sample:" ${Sline}
	echo "Flag Used:" ${FLAG}
	echo "Threads:" ${THREADS}

	module load bbc/samtools/samtools-1.9
	samtools view -b -q ${FLAG} -t ${THREADS} ${Sline} > ${base}/${DIR_NAME}/${SAMPLE}.bam
	samtools index ${base}/${DIR_NAME}/${SAMPLE}.bam
	module purge
}
