#!/usr/bin/bash

########################################
# Arguments
########################################
#$1: Directory where Samples_file.txt is located
#$2: Single or Paired end input
#$3: Extra Alignment arguments

extra_args=$3
source ~/Fresh_Scripts/./FileParser_v2.sh

########################################
# GENOMES
########################################
function mm10_genome {
    ################
    #  Genome Update Paths
    ################
    
	#STAR Genome
	#export STAR_GENOME=''
	#export STAR_GENOME_NAME=`echo ${STAR_GENOME} | cut -d'/' -f7- | awk -F '/' '{print $1}' `

	#HISAT2_Genome
	#export HISAT2_GENOME=''
	#export HISAT2_GENOME_NAME=`echo ${HISAT2_GENOME} | cut -d'/' -f7- | awk -F '/' '{print $1}' `

	#BWA Genome
	#export BWA_GENOME=''
	#export BWA_GENOME_NAME=`echo ${BWA_GENOME} | cut -d'/' -f6- | awk -F '/' '{print $1}' `

	# Kallisto Index
	#export KALLISTO_INDEX=''
	#export KALLISTO_NAME=`echo ${KALLISTO_INDEX} | cut -d'/' -f6- | awk -F '/' '{print $1}' `

	# Tophat2 LIONS
	#export TOPHAT2_LIONS_GENOME=''
	#export TOPHAT2_LIONS_NAME=`echo ${TOPHAT2_LIONS_GENOME} | cut -d '/' -f7- | awk -F '/' '{print $1}' `

	# Bowtie2
	#export BOWTIE2_GENOME=''
	#export BOWTIE2_GENOME_NAME=`echo ${BOWTIE2_GENOME} | cut -d '/' -f7- | awk -F '/' '{print $1"_""mm10"}' `

	# Kallisto
	#export KALLISTO_GENOCODE=''
	#export KALLISTO_GENCODE_NAME=`echo ${KALLISTO_GENOCODE} | cut -d'/' -f8- | awk -F '/' '{print "GencodeVM23_"$1}' `

	#export KALLISTO_REPEATS=''
	#export KALLISTO_REPEATS_NAME=`echo ${KALLISTO_REPEATS} | cut -d'/' -f8- | awk -F '/' '{print "RepeatMasker_"$1}' `
}

#function mm9_genome {
	# STAR Genome
	#export STAR_GENOME=''
	#export STAR_GENOME_NAME=`echo ${STAR_GENOME} | cut -d'/' -f7- | awk '{print $1"_""mm9"}' `
#}


########################################
# ALIGNMENT FUNCTIONS
########################################
# -- STAR -- 
function STAR_Align () {
	base=$(pwd)
	[ -d ${STAR_GENOME_NAME}_AlignedBams ] || mkdir ${STAR_GENOME_NAME}_AlignedBams
	Sample_input -l $1 -t $2 -f $3
	${MY_STAR} \
		--genomeDir ${STAR_GENOME} \
		--readFilesIn ${SFILES} \
			--readFilesCommand zcat \
		--twopassMode Basic \
		--${extra_args} \
		--outSAMtype BAM SortedByCoordinate \
		--outSAMattributes NH HI NM MD \
		--outFileNamePrefix ${base}/${STAR_GENOME_NAME}_AlignedBams/${SAMPLE}_${STAR_GENOME_NAME}
	samtools index ${base}/${STAR_GENOME_NAME}_AlignedBams/${SAMPLE}_${STAR_GENOME_NAME}*.bam
	module load bbc/fastqc/fastqc-0.11.8
	fastqc -t 4 ${base}/${STAR_GENOME_NAME}_AlignedBams/${SAMPLE}_${STAR_GENOME_NAME}*.bam
	module purge
}

# -- STAR Transposable elements Uniq mappers --

function STAR_TEUniq () {
	base=$(pwd)
	[ -d ${STAR_GENOME_NAME}_TEUniq ] || mkdir ${STAR_GENOME_NAME}_TEUniq
	Sample_input -l $1 -t $2 -f $3
	${MY_STAR} \
		--genomeDir ${STAR_GENOME} \
		--readFilesIn ${SFILES} \
			--readFilesCommand zcat \
		--runMode alignReads \
		--outFilterMultimapNmax 1 \
		--outFilterMismatchNmax 3 \
		--alignEndsType EndToEnd \
		--alignIntronMax 1 \
		--alignMatesGapMax 350 \
		--outSAMtype BAM SortedByCoordinate \
		--outFileNamePrefix ${base}/${STAR_GENOME_NAME}_TEUniq/${SAMPLE}_${STAR_GENOME_NAME}
	samtools index ${base}/${STAR_GENOME_NAME}_TEUniq/${SAMPLE}_${STAR_GENOME_NAME}*.bam
	module load bbc/fastqc/fastqc-0.11.8
	fastqc -t 4 ${base}/${STAR_GENOME_NAME}_TEUniq/${SAMPLE}_${STAR_GENOME_NAME}*.bam
	module purge

	source /home/nick.pierce/szabo-secondary/pierce_nick/anaconda2/etc/profile.d/conda.sh
	conda activate deeptools
	bamCoverage -b ${base}/${STAR_GENOME_NAME}_TEUniq/${SAMPLE}_${STAR_GENOME_NAME}*.bam \
		--normalizeUsing CPM \
		-bs 10 -o ${base}/${STAR_GENOME_NAME}_TEUniq/${SAMPLE}_CPM_binSize10bp_${STAR_GENOME_NAME}.bw
}

# -- HISAT2 -- 
function Hisat2_Align () {
	base=$(pwd)
	[ -d ${HISAT2_GENOME_NAME} ] || mkdir ${HISAT2_GENOME_NAME}
	Sample_input $1 $2
	if [ $2 == "single" ]; then
		${MY_HISAT2} -U ${Sline} -x ${HISAT2_GENOME} | samtools sort -O bam -o ${base}/${HISAT2_GENOME_NAME}/${SAMPLE}_${HISAT2_GENOME_NAME}.bam
	elif [ $2 == "paired" ]; then
		${MY_HISAT2} -1 ${SR1} -2 ${SR2} -x ${HISAT2_GENOME} | samtools sort -O bam -o ${base}/${HISAT2_GENOME_NAME}/${SAMPLE}_${HISAT2_GENOME_NAME}.bam
	else
		echo "Error with Hisat2 Alignment Setup"
	fi
	samtools index ${base}/${HISAT2_GENOME_NAME}/${SAMPLE}_${HISAT2_GENOME_NAME}.bam

	module load bbc/fastqc/fastqc-0.11.8
        fastqc -t 4 ${base}/${HISAT2_GENOME_NAME}/${SAMPLE}_${HISAT2_GENOME_NAME}.bam
	module purge
}

# -- BWA mem --
function BWAmem_Align () {
	base=$(pwd)
	[ -d bwa_${BWA_GENOME_NAME}_AlignedBams ] || mkdir bwa_${BWA_GENOME_NAME}_AlignedBams
	Sample_input -l $1 -t $2 -f $3
	if [ $2 == "single" ]; then
		${MY_BWA} mem ${BWA_GENOME} ${Sline} | samtools sort -O bam -o ${base}/bwa_${BWA_GENOME_NAME}_AlignedBams/${SAMPLE}_bwa_${BWA_GENOME_NAME}_sorted.bam
	elif [ $2 == "paired" ]; then
		${MY_BWA} mem ${BWA_GENOME} ${SR1} ${SR2} | samtools sort -O bam -o ${base}/bwa_${BWA_GENOME_NAME}_AlignedBams/${SAMPLE}_bwa_${BWA_GENOME_NAME}_sorted.bam
	else
		echo "Error with bwa Alignment setup"
	fi
	samtools index ${base}/bwa_${BWA_GENOME_NAME}_AlignedBams/${SAMPLE}_bwa_${BWA_GENOME_NAME}_sorted.bam
	samtools flagstat ${base}/bwa_${BWA_GENOME_NAME}_AlignedBams/${SAMPLE}_bwa_${BWA_GENOME_NAME}_sorted.bam > ${base}/bwa_${BWA_GENOME_NAME}_AlignedBams/${SAMPLE}_bwa_${BWA_GENOME_NAME}.flagstat
	
	module load bbc/fastqc/fastqc-0.11.8
	fastqc -t 4 ${base}/bwa_${BWA_GENOME_NAME}_AlignedBams/${SAMPLE}_bwa_${BWA_GENOME_NAME}_sorted.bam
	module purge
}

# -- bowtie2_Align --
function bowtie2_Align () {
	base=$(pwd)
	[ -d ${BOWTIE2_GENOME_NAME}_AlignedBams ] || mkdir ${BOWTIE2_GENOME_NAME}_AlignedBams
	Sample_input $1 $2
	if [ $2 == "paired" ]; then
		${MY_BOWTIE2} ${extra_args} \
			-x ${BOWTIE2_GENOME} \
			-1 ${SR1} \
			-2 ${SR2} -p 8 | samtools view -bS - | samtools sort -O bam -o ${base}/${BOWTIE2_GENOME_NAME}_AlignedBams/${SAMPLE}_${BOWTIE2_GENOME_NAME}_sorted.bam
	elif [ $2 == "single" ]; then
		${MY_BOWTIE2} ${extra_args} \
			-x ${BOWTIE2_GENOME} \
			-U ${Sline} -p 8 | samtools view -bS - | samtools sort -o ${base}/${BOWTIE2_GENOME_NAME}_AlignedBams/${SAMPLE}_${BOWTIE2_GENOME_NAME}_sorted.bam
	else
		echo "bowtie2 alignment method not recognized"
	fi
	samtools index ${base}/${BOWTIE2_GENOME_NAME}_AlignedBams/${SAMPLE}_${BOWTIE2_GENOME_NAME}_sorted.bam
	samtools flagstat ${base}/${BOWTIE2_GENOME_NAME}_AlignedBams/${SAMPLE}_${BOWTIE2_GENOME_NAME}_sorted.bam >  ${base}/${BOWTIE2_GENOME_NAME}_AlignedBams/${SAMPLE}_${BOWTIE2_GENOME_NAME}_sorted.AlignmentMetrics

	module load bbc/fastqc/fastqc-0.11.8
	fastqc -t 4 ${base}/${BOWTIE2_GENOME_NAME}_AlignedBams/${SAMPLE}_${BOWTIE2_GENOME_NAME}_sorted.bam
	module purge
}

function bowtie2_cutrun () {
	base=$(pwd)
	[ -d ${BOWTIE2_GENOME_NAME}_AlignedBams_cutrunParams ] || mkdir ${BOWTIE2_GENOME_NAME}_AlignedBams_cutrunParams
	Sample_input -l $1 -t $2 -f $3

	if [ $2 == "paired" ]; then
		${MY_BOWTIE2} \
			--threads 8 --local --very-sensitive --no-unal --no-discordant --phred33 -I 10 -X 700 \
			-x ${BOWTIE2_GENOME} \
			-1 ${SR1} \
			-2 ${SR2} | samtools view -bS - | samtools sort -O bam -o ${base}/${BOWTIE2_GENOME_NAME}_AlignedBams_cutrunParams/${SAMPLE}_${BOWTIE2_GENOME_NAME}_sorted.bam
	elif [ $2 == "single" ]; then
		${MY_BOWTIE2} \
			--threads 8 --local --very-sensitive --no-unal --no-discordant --phred33 -I 10 -X 700 \
			-x ${BOWTIE2_GENOME} \
			-U ${Sline} | samtools view -bS - | samtools sort -o ${base}/${BOWTIE2_GENOME_NAME}_AlignedBams_cutrunParams/${SAMPLE}_${BOWTIE2_GENOME_NAME}_sorted.bam
	else
		echo "bowtie2 alignment method not recognized"
	fi
	samtools index ${base}/${BOWTIE2_GENOME_NAME}_AlignedBams_cutrunParams/${SAMPLE}_${BOWTIE2_GENOME_NAME}_sorted.bam
	samtools flagstat ${base}/${BOWTIE2_GENOME_NAME}_AlignedBams_cutrunParams/${SAMPLE}_${BOWTIE2_GENOME_NAME}_sorted.bam >  ${base}/${BOWTIE2_GENOME_NAME}_AlignedBams_cutrunParams/${SAMPLE}_${BOWTIE2_GENOME_NAME}_sorted.AligmentMetrics

	module load bbc/fastqc/fastqc-0.11.8
	fastqc -t 4 ${base}/${BOWTIE2_GENOME_NAME}_AlignedBams_cutrunParams/${SAMPLE}_${BOWTIE2_GENOME_NAME}_sorted.bam
	module purg
}


# -- Kallisto Align -- STATS: NOT TESTED
#function kallisto () {
	#base=$(pwd)
	#[ -d ${KALLISTO_NAME} ] || mkdir ${KALLISTO_NAME}
	#Sample_input $1 $2
	#if [ $2 == "paired" ]; then
		#${MY_KALLISTO} quant -t 4 -i ${KALLISTO_INDEX} -o ${base}/${KALLISTO_NAME}/${SAMPLE} ${Sline}
	#if [ $2 == "single" ]; then
		#echo "Single_Ended analysis is not ready"
	#else
		#echo "kallisto method not recognized"
	#fi
#}
