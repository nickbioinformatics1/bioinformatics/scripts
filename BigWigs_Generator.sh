#!/usr/bin/bash

########################
# Arguments
########################
#$1: Location of Samples_file.txt
#$2: paired or single
#$3: Normalization Method: options RPKM, CPM, BPM, RPGC
#$4: Genome options = mm10_GENOMESIZE; mm9_GENOMESIZE; GRCh38_GENOMESIZE; GRCh37_GENOMESIZE
#$5: binSize

source ~/Fresh_Scripts/./FileParser_v2.sh
conda activate deeptools

function biggerwigger () {
	base=$(pwd)
	[ -d bigwigs ] || mkdir bigwigs
	[ -d bedgraphs ] || mkdir bedgraphs
	Sample_input -l $1 -t $2 -f $3
	echo "Sample = ${SAMPLE}"

	# Genome Sizes (RPGC Normalization)
	if [ $5 == "mm10_GENOMESIZE" ]; then
		GENOMESIZE=2652783500
	elif [ $5 == "mm9_GENOMESIZE" ]; then
		GENOMESIZE=2620345972
	elif [ $5 == "GRCh38_GENOMESIZE" ]; then
		GENOMESIZE=2913022398
	elif [ $5 == "GRCh37_GENOMESIZE" ];then
		GENOMESIZE=2864785220
	else
		echo "Could not determine Genome Size"
	fi
	genome_name=`echo $5 | cut -f1 -d "_" `

	# Generating Bigwig
	bamCoverage -b ${Sline} \
		--binSize $6 \
		--normalizeUsing $4 \
		--effectiveGenomeSize ${GENOMESIZE}  \
		-o ${base}/bigwigs/${SAMPLE}_$4_binSize$6bp_${genome_name}.bw

	# Generating bedgraph
	bamCoverage -b ${Sline} \
		--binSize $6 \
		--normalizeUsing $4 \
		--effectiveGenomeSize ${GENOMESIZE} \
		--ignoreDuplicates \
		--outFileFormat bedgraph -o ${base}/bedgraphs/${SAMPLE}_$4_binSize$6bp_${genome_name}.bedgraph
}

function biggerwigger_blacklist () {
	base=$(pwd)
	[ -d bigwigs_blackList ] || mkdir bigwigs_blackList
	[ -d bedgraphs_blackList ] || mkdir bedgraphs_blackList
	Sample_input -l $1 -t $2 -f $3
	echo "Sample = ${SAMPLE}"

	blacklist_mm10=/home/nick.pierce/szabo-secondary/pierce_nick/Genomes/BED_annotation/Encode_BlackList_mm10/Encode_BlackListRegions_mm10_sorted_merged.bed

	# Genome Sizes (RPGC Normalization)
	if [ $5 == "mm10_GENOMESIZE" ]; then
		GENOMESIZE=2652783500
	elif [ $5 == "mm9_GENOMESIZE" ]; then
		GENOMESIZE=2620345972
	elif [ $5 == "GRCh38_GENOMESIZE" ]; then
		GENOMESIZE=2913022398
	elif [ $5 == "GRCh37_GENOMESIZE" ];then
		GENOMESIZE=2864785220
	else
		echo "Could not determine Genome Size"
	fi
	genome_name=`echo $5 | cut -f1 -d "_" `

	# Generating Bigwig
	bamCoverage -b ${Sline} \
		--blackListFileName ${blacklist_mm10} \
		--ignoreDuplicates \
		--binSize $6 \
		--normalizeUsing $4 \
		--effectiveGenomeSize ${GENOMESIZE}  \
		-o ${base}/bigwigs_blackList/${SAMPLE}_$4_binSize$6bp_${genome_name}_rmBlacklist.bw

	# Generating bedgraph
	bamCoverage -b ${Sline} \
		--blackListFileName ${blacklist_mm10} \
		--ignoreDuplicates \
		--binSize $6 \
		--normalizeUsing $4 \
		--effectiveGenomeSize ${GENOMESIZE} \
		--outFileFormat bedgraph -o ${base}/bedgraphs_blackList/${SAMPLE}_$4_binSize$6bp_${genome_name}_rmBlacklist.bedgraph
}

